import React from 'react'
import { StyleSheet, Button, TextInput, View, Text } from 'react-native'
import { Formik } from 'formik'


export default function petForm() {
   return(
      <View>
         <Formik
            initialValues = {{
               petName: "",
               petPrice: '',
               petDate: '',
               petDesc: ''
            }}
            onSubmit = { values => fetch('http://10.50.38.183:3333/post', {
               method: 'POST',
               headers: {
                 Accept: 'application/json',
                 'Content-Type': 'application/json'
               },
               body: JSON.stringify(values)
             }).then(alert('Enviado com sucesso!'))
               .catch(err => alert('Algo de errado aconteceu...' )) }
         >
            { props => (
               <View>
                  <TextInput style = {styles.espaco}
                  placeholder = 'Insira aqui o nome do pet...' 
                  onChangeText = {props.handleChange('petName')} 
                  value = {props.values.petName}
                  />

                  <TextInput style = {styles.espaco}
                  placeholder = 'Insira aqui o preço da compra ou despeza...' 
                  onChangeText = {props.handleChange('petPrice')} 
                  value = {props.values.petPrice}
                  keyboardType = 'numeric'
                  />

                  <TextInput style = {styles.espaco}
                  placeholder = 'Insira aqui a data da compra...' 
                  onChangeText = {props.handleChange('petDate')} 
                  value = {props.values.petDate}
                  />

                  <TextInput style = {styles.espaco}
                  multiline
                  placeholder = 'Insira aqui uma descrição para ajudar a lembrar...' 
                  onChangeText = {props.handleChange('petDesc')} 
                  value = {props.values.petDesc}
                  />

                  <Button title ='Enviar' color = '#003333' onPress={props.handleSubmit} style = {styles.espaco}/>
               </View>
            )}
         </Formik>
      </View>
   )
}

const styles = StyleSheet.create({
   espaco: {
      marginBottom: 10,
      borderBottomColor: 'black',
      borderBottomWidth: 1
   }
 
})