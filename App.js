import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Form from './components/Form'



export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>Cadastro do Gasto</Text>
      <Form/>
      <Text>Depois confira os gastos na versao web do App {"\n"}em baixo de "Valores resgatados:".</Text>

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#138d75',  
    alignItems: 'center',
    justifyContent: 'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: "#333"
  }
});
